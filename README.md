# Ethan Hindes Showcase
This repository is intended to showcase my work for potential employers and
provide proof of acomplishment for items on my [resume](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research-Resume.pdf).

## Writeups
- [Sparse Matrix Representation](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/Spare-Matrix-Representation.pdf)
- [CNN Hyperparaneter Tuning](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/CNN-Hyperparameter-Tunning.pdf)
- [Digits Experimentation](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/DIGITS-CNN-Experimentation.pdf)
- [NVidia DLI Certification](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/Fundamentals-Certification.pdf)
- [Scaling With Horovod](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/Horovod-GPU-Scaling.pdf)
- [K Nearest Neighbors](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/K-Nearest-Neighbors.pdf)
- [MedNIST](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/MedNIST.pdf)
- [CNN Comparisons](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/Network-Comparison.pdf)
- [Numpy](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/Numpy.pdf)
- [Pandas](https://gitlab.com/ethan_hindes/showcase/-/blob/master/Research/Pandas.pdf)

## Projects
- [Speech To Text](https://gitlab.com/ethan_hindes/speech-to-text-challenge)
- [CS 2040 Programming in C and C++](https://gitlab.com/ethan_hindes/msoe-c-plus-plus)
- Academic Advising Application, for access please email me at ethanhindes@gmail.com